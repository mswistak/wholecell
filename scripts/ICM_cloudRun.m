%ICM_cloudRun
% Runs custom cloud simulations, checks its status and retrieves the data.
%
% Input:
%  - simName [char]: name of simulation
%  - parameterVals [struct]: struct containing desired parameter values
%  - parameterValsPath [char]: path to file containing desired parameter values
%  - mode [char]: function working mode; one of the following: 'cancel', 'check', 'download', 'submit'
%  - jobId [char]
%  - bucketUrl [char]: S3 bucket url where simiulation is stored
%  - outputPath [char]: the default is '../output/cloud/'
%  - alphaMatrix [double]: represents factor by which parameter value is changed; scalar and 10x3 matrix are allowed
%  - row [integer]: row indexes (1-10)
%  - column [integer]: column indexes (1-3)
%
% Not specifying any variables will result in fit cell simulation.
% Not specifying column/row will result in applying changes to all columns/rows.
%
% Output:
%  - jobId
%  - result
%  - status
%  - errMsg
%
% Example:
%  >> [jobId, result, status, errMsg] = ICM_cloudRun(...
%  'simName' 'fit', ...
%  'mode', 'submit' ...
%  );
%  >> [~, result, status, errMsg] = ICM_cloudRun(...
%  'mode', 'check', ...
%  'jobId', jobId ...
%  );
%  >> [~, ~, status, errMsg] = ICM_cloudRun(...
%  'simName' 'fit', ...
%  'mode', 'download' ...
%  );
%


function [jobId, result, status, errMsg] = ICM_cloudRun(varargin)


%%Parsing input
ip = inputParser();
validModes = {'cancel', 'check', 'download', 'submit'};

ip.addParamValue('simName', '',  @ischar);
ip.addParamValue('parameterVals', [], @(x) isstruct(x));
ip.addParamValue('parameterValsPath', [], @(x) exist(x, 'file'));
ip.addParamValue('mode', '', @(x) any(validatestring(x, validModes)));
ip.addParamValue('jobId', '', @ischar);
ip.addParamValue('bucketUrl', 's3://icm_poland.team.1961720.wcpe.sagebase.org', @ischar);
ip.addParamValue('outputPath', '../output/cloud/', @ischar);
ip.addParamValue('alphaMatrix', 1, @(x) isnumeric(x) & all(size(x) == [1 1] | size(x) == [10 3]) );
ip.addParamValue('row', 0, @(x) isnumeric(x) & all(x >= 0) & all(x <= 10) );
ip.addParamValue('column', 0, @(x) isnumeric(x) & all(x >= 0) & all(x <= 3) );

ip.parse(varargin{:});

simName = ip.Results.simName;
parameterVals = ip.Results.parameterVals;
parameterValsPath = ip.Results.parameterValsPath;
mode = ip.Results.mode;
jobId = ip.Results.jobId;
bucketUrl = ip.Results.bucketUrl;
outputPath= ip.Results.outputPath;
alphaMatrix = ip.Results.alphaMatrix;
row = ip.Results.row;
column = ip.Results.column;

if (outputPath(end) ~= '/')
        outputPath = [outputPath '/'];
end


%%Initial directory settings
tmp = pwd();
cd('../WholeCell')
cleaner = onCleanup(@() cd(tmp));


%%Mode selection
switch mode
	case 'cancel'
		[result, status, errMsg] = cancelCloudSimulation(jobId);
	case 'check'
		[result, status, errMsg] = getCloudSimulationStatus(jobId);
	case 'download'
		result = '';
		outputPath = [outputPath simName];
		if(~exist(outputPath, 'dir'))
			mkdir(outputPath);
		end
		[status, errMsg] = downloadCloudSimulationResults(...
			'simName', simName, ...
			'bucketUrl', bucketUrl, ...
			'localFolder', outputPath ...
			);
	case 'submit'
		%Applying changes
		if (isempty(simName))
			simName = datestr(now, 'yyyy_mm_dd_HH_MM_SS');
		end
		if (isempty(parameterVals) & isempty(parameterValsPath))
			cd(tmp);
			[parameterVals, ~, ~] = ICM_apply(...
				'alphaMatrix', alphaMatrix, ...
				'simName', simName, ...
				'outputPath', outputPath, ...
				'row', row, ...
				'column', column ...
				);
			cd('../WholeCell');
		else
			if (~isempty(parameterVals) & ~isempty(parameterValsPath))
				throw(MException('ICM_run:error', 'Only 1 of "parameterVals" and "parameterValsPath" can be specified.'));
			elseif (~isempty(parameterValsPath))
				parameterVals = load(parameterValsPath);
			end
		end
		
		%Post simulation
		result = '';
		[jobId, status, errMsg] = postCloudSimulation(...
			'simName', simName, ...
			'bucketUrl', bucketUrl, ...
			'parameterVals', parameterVals ...
			);
	otherwise
		throw(MException('ICM_cloudRun:invalidMode','Invalid mode. Please ensure that specified mode ("%s") is supported.', mode));
end


end
