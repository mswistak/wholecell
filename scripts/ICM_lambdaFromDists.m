%ICM_lambdaFromDists
% Calculates <lambda = v_i_true / v_i_fit>
%
% Input:
%  - d_fit [double]: parameter distance of 'fit' cell to target mutant's
%  - d_alpha [double]: parameter distance of one parameter mutant (in comparison with 'fit') to target mutant's
%  - alpha [double]: magnitude of the mutation in one parameter mutant
%
% Output:
%  - lambda [double]: magnitude of mutation comparing 'fit' and target mutant
%


function [lambda] = ICM_lambdaFromDists(d_fit, d_alpha, alpha)

N = 1972;
dd = d_fit - d_alpha;
lambda = 10.^( (N.*dd) / (log10(alpha).*2) ) .* sqrt(alpha);


end
