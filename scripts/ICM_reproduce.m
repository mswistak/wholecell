%ICM_reproduce
% Given two parents produces their offspring.
%
% Input:
%  - p1: first parent alphaMatrix
%  - p2: second parent alphaMatrix
%  - mutationRate [integer]: number of mutations applied to offspring
%
% Output:
%  - c1: first child alphaMatrix
%  - c2: second child alphaMatrix
%


function [c1, c2] = ICM_reproduce(p1, p2, mutationRate)

if (nargin < 3)
	mutationRate = 1;
end

all_ones = (p1 == 1 & p2 == 1);
all_minus = (p1 < 1 & p2 < 1);
all_plus = (p1 > 1 & p2 > 1);
other = ~(all_ones | all_minus | all_plus);

c1 = zeros(10,3) + all_ones;
c1(all_minus) = (p1(all_minus) + p2(all_minus))/2;
c1(all_plus) = (p1(all_plus) + p2(all_plus))/2;

c2 = c1;

c1(other) = p1(other);
c2(other) = p2(other);

c1 = ICM_mutate(c1, mutationRate);
c2 = ICM_mutate(c2, mutationRate);


end
