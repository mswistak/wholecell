% The Euclidean distance
function D = distEu(x,y) 
D = sum((x-y).^2).^0.5;