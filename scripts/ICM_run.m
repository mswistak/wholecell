%ICM_run
% Runs custom simulations. 
% 
% Input:
%  - lengthSeq [integer]: maximum simulation length; the default is 65000
%  - seed [integer]
%  - mode [char]: function working mode; one of the following: 'full', 'metabolic', 'both'; the default is 'full'
%  - simName [char]: name of simulation
%  - parameterVals [struct]: struct containing desired parameter values
%  - parameterValsPath [char]: path to file containing desired parameter values
%  - outputPath [char]: the default is '../output/'
%  - alphaMatrix [double]: represents factor by which parameter value is changed; scalar and 10x3 matrix are allowed
%  - nCells [integer]: number of cells to simulate; the default is 1
%  - row [integer]: row indexes (1-10)
%  - column [integer] column indexes (1-3)
%
% Not specifying any variables will result in fit cell simulation.
% Not specifying column/row will result in applying changes to all columns/rows.
%
% Output:
%  - high-throughput biological data file
%  - file containing used parameter values
%  - file containing alphaMatrix, row and column (optional)
%


function [] = ICM_run(varargin)


%%Parsing input
ip = inputParser();
validSimModes = {'full', 'metabolic', 'both'};

ip.addParamValue('lengthSec', 65000, @(x) isnumeric(x) & all(x >= 0) );
ip.addParamValue('seed', -1, @isnumeric);
ip.addParamValue('mode', 'full', @(x) any(validatestring(x, validSimModes)));
ip.addParamValue('simName', '', @ischar);
ip.addParamValue('parameterVals', [], @(x) isstruct(x));
ip.addParamValue('parameterValsPath', [], @(x) exist(x, 'file'));
ip.addParamValue('outputPath', '../output/', @ischar);
ip.addParamValue('alphaMatrix', 1, @(x) isnumeric(x) & all(size(x) == [1 1] | size(x) == [10 3]) );
ip.addParamValue('nCells', 1, @(x) isnumeric(x) & isscalar(x) & all(x > 0) );
ip.addParamValue('row', 0, @(x) isnumeric(x) & all(x >= 0) & all(x <= 10) );
ip.addParamValue('column', 0, @(x) isnumeric(x) & all(x >= 0) & all(x <= 3) );

ip.parse(varargin{:});

lengthSec = ip.Results.lengthSec;
seed = ip.Results.seed;
mode = ip.Results.mode;
simName = ip.Results.simName;
parameterVals = ip.Results.parameterVals;
parameterValsPath = ip.Results.parameterValsPath;
outputPath = ip.Results.outputPath;
alphaMatrix = ip.Results.alphaMatrix;
nCells = ip.Results.nCells;
row = ip.Results.row;
column = ip.Results.column;

if (outputPath(end) ~= '/')
	outputPath = [outputPath '/'];
end
seed_f = 0;
if (seed == -1)
	seed_f = 1;
end
if (isempty(simName))
	simName = datestr(now, 'yyyy_mm_dd_HH_MM_SS');
end


%%Applying changes
if (isempty(parameterVals) & isempty(parameterValsPath))
		[parameterVals, ~, ~] = ICM_apply(...
			'alphaMatrix', alphaMatrix, ...
			'simName', simName, ...
			'outputPath', outputPath, ...
			'row', row, ...
			'column', column ...
			);
else
	if (~isempty(parameterVals) & ~isempty(parameterValsPath))
		throw(MException('ICM_run:error', ...
			'Only 1 of "parameterVals" and "parameterValsPath" can be specified'));
	elseif (~isempty(parameterValsPath))
		parameterVals = load(parameterValsPath);
	end
end
outputPath = [outputPath simName '/'];
if (~exist(outputPath, 'dir'))
	mkdir(outputPath);
end


%%Save used parameter values
save([outputPath simName '.parameters.mat'], '-struct', 'parameterVals');


%%Setup simulation
tmp = pwd();
cd('../WholeCell');
cleaner = onCleanup(@() cd(tmp));

setWarnings();
setPath();


%%Run simulations
%if (mode == 'metabolic' | mode == 'both')
%	sim = edu.stanford.covert.cell.sim.util.CachedSimulationObjectUtil.load;
%	sim.applyAllParameters(parameterVals);
%	time = sim.state('Time');
%	met = sim.process('Metabolism');
%	transcription = sim.process('Transcription');
%	
%	sim.applyOptions(...
%		'lengthSec', 100, ...
%		'seed', seed ...
%		);
%	sim.initializeState();
%
%	logger = edu.stanford.covert.cell.sim.util.HighthroughputExperimentsLogger([outputPath simName '_metabolic' '.mat']);
%	logger.initialize(sim);
%
%	for t = 1:sim.lengthSec
%		time.values = t;
%
%		met.copyFromState();
%		met.evolveState();
%		met.copyToState();
%
%		transcription.copyFromState();
%		transcription.evolveState();
%		transcription.copyToState();
%
%		logger.append(sim);
%	end
%
%	logger.finalize(sim);
%elseif (mode == 'full' | mode == 'both')
	for i = (1:nCells)
		for ls = lengthSec
			if (seed_f)
				seed = round(rem(now,1)*1e9);
			end
			simulateHighthroughputExperiments(...
				'parameterVals', parameterVals, ...
				'seed', seed, ...
				'simPath', [outputPath  simName '_lengthSec_' num2str(ls) '_full_' num2str(i) '.mat'],  ...
				'lengthSec', ls ...
				);
		end
	end
%end


end
