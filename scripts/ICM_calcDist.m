%ICM_calcDist
% Calculate <(log(test/ref))^2>
%
% Input:
%  - alphaMatrix: 10x3 parameter transformation matrix
%
% Output:
%  - dist [double]: distance between two input vectors (scalar)
%


function dist = ICM_calcDist(aM1, aM2)

if (nargin < 2)
	aM2 = ones(10,3);
end

aM1 = aM1(:);
aM2 = aM2(:);

dist = sum(log10(aM1./aM2) .^ 2)/1972;
dist = full(dist);

end
