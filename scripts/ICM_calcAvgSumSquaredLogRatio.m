%ICM_calcAvgSumSquaredLogRatio
% Calculate <(log(test/ref))^2>
%
% Input:
%  - test: vector containing test parameter values
%  - ref: vector containing reference parameter values
%
% Output:
%  - dist [double]: parameters' distance
%


function dist = ICM_calcAvgSumSquaredLogRatio(test, ref)

test = max(-realmax, min(realmax, test));
ref  = max(-realmax, min(realmax, ref));
dist = mean(log10(test ./ ref) .^ 2);
dist = full(dist);

end
