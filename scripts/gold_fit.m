% gold_fit
% preparation of simulation results and graphical presentation

clear all
close all
clc

% load summary log into memory
% logFit = load('/Documents/Praktyka-ICM/input/fit/av/zdrowa.mat'); % simulation result of normal cell
logFit = load('../data/fit_avg_34.mat');

% logGold = load('/Documents/Praktyka-ICM/input/gold/gold.predictions.mat'); % simulation result of mutant
logGold = load('../data/gold.predictions.mat');

time = 0:65000;

%% data preparation

% single

% growth
growthGold = logGold.singleCell.growth;
avGrowthGold = mean(growthGold,1);

growthFit = logFit.singleCell.growth;
avGrowthFit = mean(growthFit(4:33,:),1);

% mass
massGold = logGold.singleCell.mass;
avMassGold = mean(massGold,1);

massFit = logFit.singleCell.mass;
avMassFit = mean(massFit(4:33,:),1);

% volume
volumeGold = logGold.singleCell.volume;
avVolumeGold = mean(volumeGold,1);

volumeFit = logFit.singleCell.volume;
avVolumeFit = mean(volumeFit(4:33,:),1);

% Replication initiation time (s)
repInitTimeGold = logGold.singleCell.repInitTime;
avRepInitTimeGold = mean(repInitTimeGold,1);

repInitTimeFit = logFit.singleCell.repInitTime;
avRepInitTimeFit = mean(repInitTimeFit,1);

% Replication termination time (s)
repTermTimeGold = logGold.singleCell.repTermTime;
%   average without NaN
    j = 1;
    repTermTime2Gold = [];
    for i = 1:length(repTermTimeGold)
        
       if isnan(repTermTimeGold(i,1))
           i = i+1;
       else
           repTermTime2Gold(j) = repTermTimeGold(i);
           j=j+1;
       end
        
    end
avRepTermTimeGold = mean(repTermTime2Gold);

repTermTimeFit = logFit.singleCell.repTermTime;
%   average without NaN
    j = 1;
    repTermTime2Fit = [];
    for i = 1:length(repTermTimeFit)
        
       if isnan(repTermTimeFit(i,1))
           i = i+1;
       else
           repTermTime2Fit(j) = repTermTimeFit(i);
           j=j+1;
       end
        
    end
avRepTermTimeFit = mean(repTermTime2Fit);

% Cell cycle length (s)
cellCycleLenGold = logGold.singleCell.cellCycleLen;
%   average without NaN
    j = 1;
    cellCycleLen2Gold = [];
    for i = 1:length(cellCycleLenGold)
        
       if isnan(cellCycleLenGold(i,1))
           i = i+1;
       else
           cellCycleLen2Gold(j) = cellCycleLenGold(i);
           j=j+1;
       end
        
    end
avCellCycleLenGold = mean(cellCycleLen2Gold);

cellCycleLenFit = logFit.singleCell.cellCycleLen;
%   average without NaN
    j = 1;
    cellCycleLen2Fit = [];
    for i = 1:length(cellCycleLenFit)
        
       if isnan(cellCycleLenFit(i,1))
           i = i+1;
       else
           cellCycleLen2Fit(j) = cellCycleLenFit(i);
           j=j+1;
       end
        
    end
avCellCycleLenFit = mean(cellCycleLen2Fit);

% average

% Metabolite concentrations (M):
metConcsGold = logGold.metConcs.mean';
stdMetConcsGold = logGold.metConcs.std';

metConcsFit = logFit.metConcs.mean';
stdMetConcsFit = logFit.metConcs.std';

% DNA-seq (DNA molecules/nt):
dnaSeqGold = logGold.dnaSeq.mean';
stdDnaSeqGold = logGold.dnaSeq.std';

dnaSeqFit = logFit.dnaSeq.mean';
stdDnaSeqFit = logFit.dnaSeq.std';

% RNA-seq (transcripts/nt):
rnaSeqGold = logGold.rnaSeq.mean';
stdRnaSeqGold = logGold.rnaSeq.std';

rnaSeqFit = logFit.rnaSeq.mean';
stdRnaSeqFit = logFit.rnaSeq.std';

% ChIP-seq (protein molecules/nt):
chipSeqGold = logGold.chipSeq.mean;
stdChipSeqGold = logGold.chipSeq.std;

chipSeqFit = logFit.chipSeq.mean;
stdChipSeqFit = logFit.chipSeq.std;

% RNA expression array (M):
rnaArrayGold = logGold.rnaArray.mean';
stdRnaArrayGold = logGold.rnaArray.std';

rnaArrayFit = logFit.rnaArray.mean';
stdRnaArrayFit = logFit.rnaArray.std';

% Protein expression array (M):
protArrayGold = logGold.protArray.mean';
stdProtArrayGold = logGold.protArray.std';

protArrayFit = logFit.protArray.mean';
stdProtArrayFit = logFit.protArray.std';

% Metabolic reaction fluxes (rxn/s/gDCW):
reFluxGold = logGold.rxnFluxes.mean';
stdReFluxGold = logGold.rxnFluxes.std';

reFluxFit = logFit.rxnFluxes.mean';
stdReFluxFit = logFit.rxnFluxes.std';

%% plots
figure()

subplot(3, 1, 1);
plot(time, avGrowthGold, 'r', time, avGrowthFit, 'b');
xlabel('Time (s)');
ylabel('Growth (g/s)');

subplot(3, 1, 2);
plot(time, avMassGold, 'r', time, avMassFit, 'b');
xlabel('Time (s)');
ylabel('Mass (g)');

subplot(3, 1, 3);
plot(time, avVolumeGold, 'r', time, avVolumeFit, 'b');
xlabel('Time (s)');
ylabel('Volume (L)');

% saveas(gcf,'figure1','png')
% saveas(gcf,'figure1','fig')

figure()

% Metabolite concentrations (M): 
subplot(3, 1, 1); 
plot(metConcsGold,'.r') % H2O stands out
hold on
plot(metConcsFit,'.b')
xlabel('Metabolite');
ylabel('Metabolite concentrations (M)');

% in log.dnaSeq: DNA-seq (DNA molecules/nt)
subplot(3, 1, 2);
plot(dnaSeqGold,'.r');
hold on
plot(dnaSeqFit,'.b');
xlabel('');
ylabel('DNA-seq (DNA molecules/nt)');

% in log.rnaSeq: RNA-seq (transcripts/nt)
subplot(3, 1, 3);
plot(rnaSeqGold,'.r');
hold on
plot(rnaSeqFit,'.b');
xlabel('');
ylabel('transcripts/nt');

% in log.rnaArray: RNA expression array (M)
figure()
subplot(3,1,1)
plot(rnaArrayGold,'.r')
hold on
plot(rnaArrayFit,'.b')
xlabel('genes')
ylabel('RNA expression (M)')

% in log.protArray: Protein expression array (M)
subplot(3,1,2)
plot(protArrayGold,'.r')
hold on
plot(protArrayFit,'.b')
xlabel('protein-coding genes')
ylabel('Protein expression (M)')

% in log.rxnFluxes: Metabolic reaction fluxes (rxn/s/gDCW)
subplot(3,1,3)
plot(reFluxGold,'.r')
hold on
plot(reFluxFit,'.b')
xlabel('reaction')
ylabel('fluxes (rxn/s/gDCW)')

% saveas(gcf,'figure3','png')
% saveas(gcf,'figure3','fig')
