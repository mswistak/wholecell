%ICM_calcAvgNormSquaredDiff
% Calculate <((mean(ref) - mean(test))^2) / var(ref)>
%
% Input:
%  - test: test prediction vector
%  - ref: reference prediction vector
%
% Output:
%  - dist [double]: predictions' distance
%


function dist = ICM_calcAvgNormSquaredDiff(test, ref)

tfs = (ref.std ~= 0);

dist = mean( ((ref.mean(tfs) - test.mean(tfs)) .^ 2) ./ (ref.std(tfs) .^ 2) );

dist = full(dist);


end
