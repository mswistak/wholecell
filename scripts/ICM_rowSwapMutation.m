%ICM_rowSwapMutation
% Swaps two rows in alphaMatrix
%
% Input:
%  - alphaMatrix
%  - i: row index (optional)
%
% Output:
%  - alphaMatrix: mutated input
%


function [alphaMatrix] = ICM_rowSwap(alphaMatrix, i)

if (nargin < 2)
	i = randi(10,1,1);
end

i2 = randi(10,1,1);
tmp = alphaMatrix(i,1:3);
alphaMatrix(i,1:3) = alphaMatrix(i2,1:3);
alphaMatrix(i2,1:3) = tmp;


end
