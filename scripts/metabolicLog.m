% metabolicLog
% sub-model simulation results into a single structure- reaction

clear all
close all

%cd('/home/users/matc89/WholeCell') % the directory path of cell
cd('../WholeCell')

% setup MATLAB:
setWarnings(); % warnings
setPath(); % path

import edu.stanford.covert.cell.sim.util.*; % import classes

sim = CachedSimulationObjectUtil.load(); % load simulation object with default parameter values

reactionKinetics = sim.getMetabolicReactionKinetics();

pRK = [reactionKinetics.Tmk.for;...
reactionKinetics.Fba.for;...
reactionKinetics.MetK.for;...
reactionKinetics.Pgi.for;...
reactionKinetics.AceE.for;...
reactionKinetics.Pta.for;...
reactionKinetics.CmkA2.for;...
reactionKinetics.AckA.for;...
reactionKinetics.Eno.for;...
reactionKinetics.TpiA.for];

w = [0.1 0.5 1 2 10]; % vector changes in parameter values

% parameter values
Tmk = w.*pRK(1);
Fba = w.*pRK(2);
MetK = w.*pRK(3);
Pgi = w.*pRK(4);
AceE = w.*pRK(5);
Pta = w.*pRK(6);
CmkA2 = w.*pRK(7);
AckA = w.*pRK(8);
Eno = w.*pRK(9);
TpiA = w.*pRK(10);

parametryLabel = {'Tmk' 'Fba' 'MetK' 'Pgi' 'AceE' 'Pta' 'CmkA2' 'AckA' 'Eno' 'TpiA'};
parametry = [Tmk; Fba; MetK; Pgi; AceE; Pta; CmkA2; AckA; Eno; TpiA];

%path = '/home/users/matc89/WholeCell/output/symulacje/parametry_metabol/';
path = '../output/metabolic/';

reaction = cell(length(parametryLabel),length(w));
for id=1:length(parametryLabel)

	path2 = [path parametryLabel{id} '/']; % path to the simulation results


	for k=1:length(w)

		name_mat = [parametryLabel{id} '_' num2str(parametry(id,k)) '.' 'mat']; % mat-file name

		name = [parametryLabel{id} '_' num2str(parametry(id,k))];

		data = load([path2 name_mat]); % take the results of the simulation

		fluxs = mean(data.fluxs,2);
		cellCycleLength = data.met.cellCycleLength;
		growth = data.growth;

		reaction{id,k} = {fluxs growth cellCycleLength name}; 

		clear('data');

	end
end


save([path 'all_reaction.mat'],'reaction') % save the needed data
