%ICM_searchLabs
% Gives indexes of searched labels
%
% Input:
%  - labs [cell]: set of labels (should be sorted)
%  - search [cell]: searched terms
%
% Output:
%  - idxs [integer]: indexes of searched terms (sorted) in labs
%


function [idxs] = ICM_searchLabs(labs, search)

[search, ~] = sort(search);

j = 1;
idxs = [];
for i = 1:length(labs)
	if (j > length(search))
		break;
	end
	if (strcmp(labs{i}, search{j}))
		idxs = [idxs i];
		j = j+1;
	end
end

if (j < length(search))
	fprintf('"%s" was not found\n', search{j});
end


end
