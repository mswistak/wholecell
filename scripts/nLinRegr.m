% nLinRegr
% function performing nonlinear regression
% input:
%   - parametryRK: matrix of the parameter values
%   - D_gr: prediction Euclidean distance- growth
%   - D_ma: prediction Euclidean distance- mass
%   - D_vo: Prediction Euclidean distance- volume
%   - r: number of parameters
%   - parametryLabel: parameter names

function nLinRegr(parametryRK, D_gr, D_ma, D_vo, r, parametryLabel)
xx = [];
a = [];
for i = 1:r
  
xp = parametryRK(i,:);
x = linspace(min(xp),max(xp),100);
yp_gr = D_gr(i,:);
yp_gr = yp_gr(1:5);
yp_ma = D_ma(i,:);
yp_ma = yp_ma(1:5);
yp_vo = D_vo(i,:);
yp_vo = yp_vo(1:5);

a_gr=polyfit(xp,yp_gr,2);
y_gr=polyval(a_gr,x);
a_ma=polyfit(xp,yp_ma,2);
y_ma=polyval(a_ma,x);
a_vo=polyfit(xp,yp_vo,2);
y_vo=polyval(a_vo,x);

xx(i,:) = x;
a(:,:,i) = [a_gr; a_ma; a_vo];

% plots
figure(r+i)
subplot(3,1,1)
plot(xp,yp_gr,'o',x,y_gr,'r');
title(['The distance between the simulated and the mutant for changes ' parametryLabel{i}])
xlabel('parameter value');
ylabel('distance for growth');
legend('measuring points', 'polynominal of degree 2')

subplot(3,1,2)
plot(xp,yp_ma,'o',x,y_ma,'r');
xlabel('parameter value');
ylabel('distance for mass');

subplot(3,1,3)
plot(xp,yp_vo,'o',x,y_vo,'r');
xlabel('parameter value');
ylabel('distance for volume');
     
end

