%ICM_swap2ColumnsIn2RowsMutation
% Swaps values of alphaMatrix in a row between two columns. It is done in two rows to balance the changes.
%
% Input:
%  - alphaMatrix
%  - i, j: indices (optional)
%
% Output:
%  - alphaMatrix: mutated input
%


function [alphaMatrix] = ICM_swap2ColumnsIn2RowsMutation(alphaMatrix, i, j)

if (nargin < 3)
	i = randi(10,1,1);
	j = randi(3,1,1);
end

t1 = alphaMatrix(i,j);
while 1
	i2 = randi(10,1,1);
	j2 = randi(3,1,1);
	
	t2 = alphaMatrix(i2,j2);
	d1 = alphaMatrix(i,j2);
	d2 = alphaMatrix(i2,j);
		
	if ( ((t1 == 1 & t2 == 1) | (t1 ~= 1 & t2 ~= 1)) & ((d1 == 1 & d2 == 1) | (d1 ~= 1 & d2 ~= 1)) )
		alphaMatrix(i,j) = d1;
		alphaMatrix(i2,j) = t2;
		alphaMatrix(i,j2) = t1;
		alphaMatrix(i2,j2) = d2;
		return;
	end
end


end
