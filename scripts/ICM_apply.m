%ICM_apply
% Applies changes to parameter values.
%
% Input:
%  - alphaMatrix [double]: represents factor by which parameter value is changed; scalar and 10x3 matrix are allowed
%  - simName [char]: name of simulation
%  - outputPath [char]: base output path; the default is '../output/'
%  - row [integer]: row indexes (1-10)
%  - column [integer]: column indexes (1-3)
%  - noWrite [integer]: flag for writing alphaParameters
%
% Output:
%  - parameterVals
%  - parameterVector
%  - outputPath
%
% Also saves alphaMatrix, column and row as a struct to file '<simName>.alphaParameters.mat'.
%
% Example:
%  >> alphaMatrix = ones(10, 3);
%  >> row = [3 5 8 10];
%  >> alphaMatrix(row, 3) = alphaMatrix(row, 3) + linspace(-.5, .5, length(row));
%  >> [parameterVals, outputPath] = ICM_apply(...
%  'alphaMatrix', alphaMatrix, ...
%  'column', 3, ...
%  'row', row ...
%  );
%


function [parameterVals, parameterVector, outputPath] = ICM_apply(varargin)


%%Parsing input
ip = inputParser();

ip.addParamValue('alphaMatrix', 1, @(x) isnumeric(x) & all(size(x) == [1 1] | size(x) == [10 3]) );
ip.addParamValue('simName', '', @ischar);
ip.addParamValue('outputPath', '../output/', @ischar);
ip.addParamValue('row', 0, @(x) isnumeric(x) & all(x >= 0) & all(x <= 10) );
ip.addParamValue('column', 0, @(x) isnumeric(x) & all(x >= 0) & all(x <= 3) );
ip.addParamValue('noWrite', 0)

ip.parse(varargin{:});

alphaMatrix = ip.Results.alphaMatrix;
outputPath = ip.Results.outputPath;
simName = ip.Results.simName;
row = ip.Results.row;
column = ip.Results.column;
noWrite = ip.Results.noWrite;

if (outputPath(end) ~= '/')
	outputPath = [outputPath '/'];
end


%%Initial settings
tmp = pwd();
cd('../WholeCell');
cleaner = onCleanup(@() cd(tmp));

setWarnings();
setPath();
sim = edu.stanford.covert.cell.sim.util.CachedSimulationObjectUtil.load();


%%Prepare to apply changes
if (any(alphaMatrix ~= 1))
	probs = sim.getRnaPolTuBindingProbs();
	halfLives = sim.getRnaHalfLives();
	kcats = sim.getMetabolicReactionKinetics();

	ids = {
		'TU_003' 'TU_003' 'Tmk'
		'TU_012' 'TU_011' 'Fba'
		'TU_028' 'TU_027' 'MetK'
		'TU_070' 'TU_069' 'Pgi'
		'TU_184' 'TU_180' 'AceE'
		'TU_209' 'TU_203' 'Pta'
		'TU_245' 'TU_233' 'CmkA2'
		'TU_272' 'TU_260' 'AckA'
		'TU_306' 'TU_294' 'Eno'
		'TU_319' 'TU_307' 'TpiA'
		};

	par = [];
	d = size(ids);
	for i = 1:(d(1))
		par = [par; probs.(ids{i,1}) halfLives.(ids{i,2}) kcats.(ids{i,3}).for];
	end


%%Applying changes
	if (row == 0)
		row = (1:10);
	end
	if (column == 0)
		column = (1:3);
	end
	if (isscalar(alphaMatrix))
		aM = ones(10,3);
		aM(row,column) = alphaMatrix;
		alphaMatrix = aM;
	else
		alphaMatrix(setdiff(1:10, row),:) = ones;
		alphaMatrix(:,setdiff(1:3, column)) = ones;
	end

	for c = column
		if (c == 1)
			sim.applyRnaPolTuBindingProbs(...
				cell2struct(num2cell(alphaMatrix(row, c).*par(row, c)), ids(row, c)) ...
				);
		elseif (c == 2)
			sim.applyRnaHalfLives(...
				cell2struct(num2cell(alphaMatrix(row, c).*par(row, c)), ids(row, c)) ...
				);
		elseif (c == 3)
			value = struct();
			for r = row
				value.(ids{r,c}) = struct('for', alphaMatrix(r,c).*par(r,c));
			end
			sim.applyMetabolicReactionKinetics(value);
		end
	end
end


%%Get final parameter values
parameterVals = sim.getAllParameters();
parameterVector = [
	parameterVals.states.Rna.halfLives(sim.state('Rna').matureIndexs)
	-parameterVals.processes.Metabolism.enzymeBounds(:,1)
	parameterVals.processes.Metabolism.enzymeBounds(:,2)
	parameterVals.processes.Transcription.transcriptionUnitBindingProbabilities
	];


%%Save alphaParameters
if (noWrite == 0)
	if (isempty(simName))
		simName = datestr(now, 'yyyy_mm_dd_HH_MM_SS');
	end
	outputPath = [outputPath simName '/'];
	if (~exist(outputPath, 'dir'))
		mkdir(outputPath);
	end

	alphaParameters = struct('row', row, 'column', column, 'alphaMatrix', alphaMatrix);
	save([outputPath simName '.alphaParameters.mat'], '-struct', 'alphaParameters');
end


end
