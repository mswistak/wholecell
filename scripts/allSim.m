%allSim
%script prepares and runs simulations of whole cells
%Input:
%	-col [integer]: column number; 1- PromAffinity, 2- HalfLife, 3- RxnKcat (int 1-3)
%	-gen: vector of indices genes (int 1-10)
%Output:
%	-mat-files with the simulation results
%	-mat-files with the default parameters

clear all
close all

col = 3; % select 1, 2 or 3
gen = [9:10]; % select 1-10

%cd('/home/users/matc89/WholeCell') % path to the cell directory
cd('../WholeCell')

% setup MATLAB:
setWarnings(); % warnings
setPath(); % path

t = 65000; % simulation time


import edu.stanford.covert.cell.sim.util.*; % import classes

% parameter names
parametryLabel = {'Tmk' 'Fba' 'MetK' 'Pgi' 'AceE' 'Pta' 'CmkA2' 'AckA' 'Eno' 'TpiA'};
parametryLabel2 = {'TU_003' 'TU_011' 'TU_027' 'TU_069' 'TU_180' 'TU_203' 'TU_233' ...
		   'TU_260' 'TU_294' 'TU_307'};


[sim, kbWID] = CachedSimulationObjectUtil.load(); % load simulation object with default parameter values

sim.applyOptions('lengthSec', t); % set option values 


% get current parameter values
OA = sim.getRnaPolTuBindingProbs();

pOA = [OA.TU_003;... 
OA.TU_011;...
OA.TU_027;...
OA.TU_069;...
OA.TU_180;...
OA.TU_203;...
OA.TU_233;...
OA.TU_260;...
OA.TU_294;...
OA.TU_307];

OHL = sim.getRnaHalfLives();

pOHL = [OHL.TU_003;...
OHL.TU_011;...
OHL.TU_027;...
OHL.TU_069;...
OHL.TU_180;...
OHL.TU_203;...
OHL.TU_233;...
OHL.TU_260;...
OHL.TU_294;...
OHL.TU_307];

RK = sim.getMetabolicReactionKinetics();

pRK = [RK.Tmk.for;...
RK.Fba.for;...
RK.MetK.for;...
RK.Pgi.for;...
RK.AceE.for;...
RK.Pta.for;...
RK.CmkA2.for;...
RK.AckA.for;...
RK.Eno.for;...
RK.TpiA.for];

w = [0.0660 0.5330 1.4530 1.906]; % changes' vector in parameter values


TU_003_oa = w.*pOA(1); 
TU_011_oa = w.*pOA(2);
TU_027_oa = w.*pOA(3);
TU_069_oa = w.*pOA(4);
TU_180_oa = w.*pOA(5);
TU_203_oa = w.*pOA(6);
TU_233_oa = w.*pOA(7);
TU_260_oa = w.*pOA(8);
TU_294_oa = w.*pOA(9);
TU_307_oa = w.*pOA(10);
% parameter values of PromAffinity
parametryOA = [TU_003_oa;TU_011_oa;TU_027_oa;TU_069_oa;TU_180_oa;TU_203_oa;TU_233_oa;TU_260_oa;TU_294_oa;TU_307_oa];

TU_003_ohl = w.*pOHL(1);
TU_011_ohl = w.*pOHL(2);
TU_027_ohl = w.*pOHL(3);
TU_069_ohl = w.*pOHL(4);
TU_180_ohl = w.*pOHL(5);
TU_203_ohl = w.*pOHL(6);
TU_233_ohl = w.*pOHL(7);
TU_260_ohl = w.*pOHL(8);
TU_294_ohl = w.*pOHL(9);
TU_307_ohl = w.*pOHL(10);
% parameter values of HalfLife
parametryOHL = [TU_003_ohl;TU_011_ohl;TU_027_ohl;TU_069_ohl;TU_180_ohl;TU_203_ohl;TU_233_ohl;TU_260_ohl;TU_294_ohl;TU_307_ohl];

Tmk = w.*pRK(1);
Fba = w.*pRK(2);
MetK = w.*pRK(3);
Pgi = w.*pRK(4);
AceE = w.*pRK(5);
Pta = w.*pRK(6);
CmkA2 = w.*pRK(7);
AckA = w.*pRK(8);
Eno = w.*pRK(9);
TpiA = w.*pRK(10);
% parameter values of RxnKcat
parametryRK = [Tmk; Fba; MetK; Pgi; AceE; Pta; CmkA2; AckA; Eno; TpiA];

for id = 1:length(gen)

	if col==1 % column of PromAffinity
		%simBatchDir = ['output/symulacje/parametry_all/column1/'  parametryLabel2{gen(id)}]; % simulation batch output directory
		simBatchDir = ['../output/single/column1/'  parametryLabel2{gen(id)}];
		if ~isdir(simBatchDir)
		mkdir('../output', 'single')
		%mkdir('output/symulacje', 'parametry_all');
		%mkdir('output/symulacje/parametry_all', 'column1');
		mkdir('../output/single', 'column1');
		%mkdir('output/symulacje/parametry_all/column1', parametryLabel2{gen(id)}) % create simulation batch output directory
		mkdir('../output/single/column1', parametryLabel2{gen(id)}) % create simulation batch output directory

		end


               for k = 1:length(w)

	       		simBatchDir2 = [simBatchDir '/' num2str(parametryOA(gen(id),k))]; % simulation output directory

			if ~isdir(simBatchDir2)
			mkdir([simBatchDir '/'], num2str(parametryOA(gen(id),k))) % create simulation output directory
			mkdir(simBatchDir2, 'single')
			end

			[sim, kbWID] = CachedSimulationObjectUtil.load(); % load simulation object with default parameter values
			% set parameter value
			sim.applyOptions('lengthSec', t);
			
			% modify parameter values
			sim.applyRnaPolTuBindingProbs(struct(...
				parametryLabel2{gen(id)},parametryOA(gen(id),k)...
				));
		
			% get a struct containing the values of all of the simulation's parameters
			parameterVals = sim.getAllParameters();
		

			for i = 1:2 % cardinality of the population

                        % simulate and measure individual in silico cells
			simulateHighthroughputExperiments(...
				'seed', round(rem(now,1)*1e9), ...
				'parameterVals', parameterVals, ...
				'simPath', sprintf(strcat(simBatchDir2,'/single','/sim-%d.mat'), i) ...
				);
			end
			
			% calculate population averages
			averageHighthroughputExperiments(...
				'simPathPattern', strcat(simBatchDir2,'/single','/sim-*.mat'), ...
				'avgValsPath', strcat(simBatchDir2,'/sim-av.mat') ...
				);

		end

	end
	
	if col==2 % column of HalfLife

                %simBatchDir = ['output/symulacje/parametry_all/column2/'  parametryLabel2{gen(id)}]; % simulation batch output directory
                simBatchDir = ['../output/single/column2/'  parametryLabel2{gen(id)}];
                if ~isdir(simBatchDir)
                mkdir('../output', 'single')
                %mkdir('output/symulacje', 'parametry_all');
                %mkdir('output/symulacje/parametry_all', 'column2');
                mkdir('../output/single', 'column2');
                %mkdir('output/symulacje/parametry_all/column2', parametryLabel2{gen(id)}) % create simulation batch output directory
                mkdir('../output/single/column2', parametryLabel2{gen(id)}) % create simulation batch output directory
		end

               for k = 1:length(w)

	       		simBatchDir2 = [simBatchDir '/' num2str(parametryOHL(gen(id),k))];

			if ~isdir(simBatchDir2)
				mkdir([simBatchDir '/'], num2str(parametryOHL(gen(id),k)))
				mkdir(simBatchDir2, 'single')
			end

			[sim, kbWID] = CachedSimulationObjectUtil.load(); % load simulation object with default parameter values
			
			% set parameter value
			sim.applyOptions('lengthSec', t);

			sim.applyRnaHalfLives(struct(...
				parametryLabel2{gen(id)},parametryOHL(gen(id),k)...
				));

			% get a struct containing the values of all of the simulation's parameters
			parameterVals = sim.getAllParameters();

			for i = 1:2

			% simulate and measure individual in silico cells
			simulateHighthroughputExperiments(...
				'seed', round(rem(now,1)*1e9), ...
				'parameterVals', parameterVals, ...
				'simPath', sprintf(strcat(simBatchDir2,'/single','/sim-%d.mat'), i) ...
				);
			end
			
			% calculate population averages
			averageHighthroughputExperiments(...
				'simPathPattern', strcat(simBatchDir2,'/single','/sim-*.mat'), ...
				'avgValsPath', strcat(simBatchDir2,'/sim-av.mat') ...
				);
		end
	end
	
	if col==3 % column of RxnKcat
               
                %simBatchDir = ['output/symulacje/parametry_all/column3/'  parametryLabel{gen(id)}]; % simulation batch output directory
                simBatchDir = ['../output/single/column3/'  parametryLabel{gen(id)}];
                if ~isdir(simBatchDir)
                mkdir('../output', 'single')
                %mkdir('output/symulacje', 'parametry_all');
                %mkdir('output/symulacje/parametry_all', 'column3');
                mkdir('../output/single', 'column3');
                %mkdir('output/symulacje/parametry_all/column3', parametryLabel{gen(id)}) % create simulation batch output directory
                mkdir('../output/single/column3', parametryLabel{gen(id)}) % create simulation batch output directory
		end

		for k = 1:length(w)

                       simBatchDir2 = [simBatchDir '/' num2str(parametryRK(gen(id),k))];

             
			if ~isdir(simBatchDir2)
			mkdir([simBatchDir '/'], num2str(parametryRK(gen(id),k)))
			mkdir(simBatchDir2, 'single')
			end

			[sim, kbWID] = CachedSimulationObjectUtil.load(); % load simulation object with default parameter values

			% set parameter value
			sim.applyOptions('lengthSec', t);
			
			sim.applyMetabolicReactionKinetics(struct(...
				parametryLabel{gen(id)}, struct('for', parametryRK(gen(id),k)) ...
				));
			
			% get a struct containing the values of all of the simulation's parameters
			parameterVals = sim.getAllParameters();
			
			for i = 1:1

                        % simulate and measure individual in silico cells
			simulateHighthroughputExperiments(...
				'seed', round(rem(now,1)*1e9), ...
				'parameterVals', parameterVals, ...
				'simPath', sprintf(strcat(simBatchDir2,'/single','/sim-%d.mat'), i) ...
				);
			end
                
			% calculate population averages
		        %averageHighthroughputExperiments(...
			%	'simPathPattern', strcat(simBatchDir2,'/single','/sim-*.mat'), ...
			%	'avgValsPath', strcat(simBatchDir2,'/sim-av.mat') ...
			%	);
		end
	end
end

%save('output/symulacje/parameters.mat','pOA','pOHL','pRK')
save('../output/single/parameters.mat','pOA','pOHL','pRK')


exit

