%ICM_prepareData
% Prepares 2 csv files containing parameters and distances of all jobs in target directory.
%
% Input:
%  - inputPath [char]: path to jobs' superdirectory (default is '../output/cloud/')
%  - outputPath [char]: (the default is '../data/')
%
% Output:
%  - H.csv, H.mat: file containing a 30xN matrix (N equals to number of jobs in target superdirectory)
%  - s.csv, H.mat: parameter distances to target mutant (1xN)
%  - labs.mat: file containing cell with jobs' labels (for easy identification)
%


function [H, s labs] = ICM_prepareData(inputPath, outputPath)


if (nargin < 1 | isempty(inputPath))
	inputPath = '../output/cloud/';
end

if (nargin < 2 | isempty(outputPath))
	outputPath = '../data/';
end

if (inputPath(end) ~= '/')
	inputPath = [inputPath '/'];
end

if (outputPath(end) ~= '/')
	outputPath = [outputPath '/'];
end

dirs = dir(inputPath);
dims = size(dirs);

H = [];
s = [];
labs = {};
for i = 1:(dims(1))
	try
		name = dirs(i).name;
		if (name(1) ~= '.')
			d = load([inputPath name '/' name '.distances.mat']);
			load([inputPath name '/' name '.alphaParameters.mat']);

			s = [s d.parameter];
			H = [H alphaMatrix(:)];
			labs = [labs name];
		end
	catch err
		fprintf('%s\n', err.message)
	end
end

csvwrite([outputPath 'H.csv'], H)
save([outputPath 'H.mat'], 'H')
csvwrite([outputPath 's.csv'], s)
save([outputPath 's.mat'], 's')
save([outputPath 'labs.mat'], 'labs')


end
