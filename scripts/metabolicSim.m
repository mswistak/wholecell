% metabolicSim
% simulating the metabolic sub-model

clear all
close all

%cd('/home/users/matc89/WholeCell') % the directory path of cell 
cd('../WholeCell')

% setup MATLAB:
setWarnings(); % warnings
setPath(); % path

t = 2;%50000; % simulation time

import edu.stanford.covert.cell.sim.util.*; % import classes
import edu.stanford.covert.cell.sim.state.*; 


sim = CachedSimulationObjectUtil.load(); % load simulation object with default parameter values

reactionKinetics = sim.getMetabolicReactionKinetics();

pRK = [reactionKinetics.Tmk.for;...
reactionKinetics.Fba.for;...
reactionKinetics.MetK.for;...
reactionKinetics.Pgi.for;...
reactionKinetics.AceE.for;...
reactionKinetics.Pta.for;...
reactionKinetics.CmkA2.for;...
reactionKinetics.AckA.for;...
reactionKinetics.Eno.for;...
reactionKinetics.TpiA.for];

w = [0.1 0.5 1 2 10]; % vector changes in parameter values

% parameter values
Tmk = w.*pRK(1);
Fba = w.*pRK(2);
MetK = w.*pRK(3);
Pgi = w.*pRK(4);
AceE = w.*pRK(5);
Pta = w.*pRK(6);
CmkA2 = w.*pRK(7);
AckA = w.*pRK(8);
Eno = w.*pRK(9);
TpiA = w.*pRK(10);

parametryLabel = {'Tmk' 'Fba' 'MetK' 'Pgi' 'AceE' 'Pta' 'CmkA2' 'AckA' 'Eno' 'TpiA'}; % parameter names
parametry = [Tmk; Fba; MetK; Pgi; AceE; Pta; CmkA2; AckA; Eno; TpiA];


for id = 1:length(parametryLabel)
	% create simulation output directory
	%simBatchDir = ['output/symulacje/parametry_metabol/' parametryLabel{id}]; % simulation batch output directory
	simBatchDir = ['../output/metabolic/' parametryLabel{id}];
	if ~isdir(simBatchDir)
		%mkdir('output', 'symulacje')
		%mkdir('output/symulacje', 'parametry_metabol');
		%mkdir('output/symulacje/parametry_metabol',parametryLabel{id}); % create simulation batch output directory
		mkdir('../output','metabolic')
		mkdir('../output/metabolic',parametryLabel{id});
	end

	for k = 1:length(w)
										    
		sim = CachedSimulationObjectUtil.load(); % load simulation object with default parameter values
		% set simulation options
		sim.applyOptions('seed', 1);
		% set parameter values
		sim.applyOptions('lengthSec', t);

		% set simulation parameter
		sim.applyMetabolicReactionKinetics(struct(... 
		parametryLabel{id}, struct('for', parametry(id,k)) ...
		));

		% get handle to metabolism sub-model
		met = sim.process('Metabolism');
		
		% get handle to metabolic reaction state
		fzr = sim.state('FtsZRing');
		h = sim.state('Host');
		rp = sim.state('RNAPolymerase');
		s = sim.state('Stimulus');
		mr = sim.state('MetabolicReaction');
		ch = sim.state('Chromosome');
		tr = sim.state('Transcript');
		pp = sim.state('Polypeptide');
		cg = sim.state('CellGeometry');
		m = sim.state('Metabolite');
		rna = sim.state('Rna');
		pm = sim.state('ProteinMonomer');
		pc = sim.state('ProteinComplex');
		cm = sim.state('CellMass');
		
		% simulate dynamics for t
		growth = zeros(1,t);
		fluxs = zeros(645,t);

		for i = 1:t
			met.evolveState();
			growth(1,i) = mr.growth;
			fluxs(:,i) = mr.fluxs;
		end


		name = [parametryLabel{id} '_' num2str(parametry(id,k)) '.' 'mat'];
		save([simBatchDir '/' name],'growth','fluxs','cm','met','mr','sim') % save the results

	end
end

exit
