% singleParameters
% comparison of simulation results for changes in the value of individual 
% parameters; stability parameters 

clear all
close all
clc

time = 0:65000;
% load default parameter values
% load('/Documents/Praktyka-ICM/input/parameters.mat');
load('../output/single/parameters.mat');

parametryLabel = {'Tmk' 'Fba' 'MetK' 'Pgi' 'AceE' 'Pta' 'CmkA2' 'AckA' 'Eno' 'TpiA'};

w = [0.0660 0.5330 1 1.4530 1.9060]; % changes' vector in parameter values

parametryRK = [];
for j = 1:length(pRK)
    parametryRK(j,:) = w*pRK(j);
end

% load mutant and fit into memory
% log_cel = load('/Documents/Praktyka-ICM/input/gold/gold.predictions.mat'); % mutant
% log_dom = load('/Documents/Praktyka-ICM/input/fit/av/zdrowa.mat'); % fit
log_cel = load('../data/gold.predictions.mat'); % mutant
log_dom = load('../data/fit_avg_34.mat'); % fit


log_all = {};
for id = 1:length(parametryLabel)
   
%     path to the simulation results
%	  simBatchDir = ['/Documents/Praktyka-ICM/input/all/' parametryLabel{id} '/'];
    simBatchDir = ['../output/single/column1/' parametryLabel{id} '/'];
	
    log_test1 = load([simBatchDir num2str(parametryRK(id,1)) '/single/sim-1.mat']);
    log_test2 = load([simBatchDir num2str(parametryRK(id,2)) '/single/sim-1.mat']);
    log_test4 = load([simBatchDir num2str(parametryRK(id,4)) '/single/sim-1.mat']);
    log_test5 = load([simBatchDir num2str(parametryRK(id,5)) '/single/sim-1.mat']);
    log_all(id,:) = {log_test1 log_test2 log_dom log_test4 log_test5 log_cel};
%                   { value1   value2   valueFit  value4   value5  valueMut}

end

% The Euclidean distance

[r c] = size(log_all);

growth = zeros(c,length(time),length(parametryLabel));
mass = zeros(c,length(time),length(parametryLabel));
volume = zeros(c,length(time),length(parametryLabel));

D_gr = [];
D_ma = [];
D_vo = [];

% growth
for i = 1:r
    
    growth_mut = log_all{i,6}.singleCell.growth;
    av_growth_mut = mean(growth_mut,1);

    growth_dom = log_all{i,3}.singleCell.growth;
    av_growth_dom = mean(growth_dom(4:33,:),1);

    for j = 1:c
        if j == 3
            growth(j,:,i) = av_growth_dom;
           
        elseif j == 6
            growth(j,:,i) = av_growth_mut;
        else
        growth_test = log_all{i,j}.singleCell.growth;
        av_growth_test = mean(growth_test,1);
        growth(j,:,i) = av_growth_test;
        end
        
        D_gr(i,j) = distEu(av_growth_mut, growth(j,:,i)); % the Euclidean distance
    end
end

% mass
for i = 1:r
    
    mass_mut = log_all{i,6}.singleCell.mass;
    av_mass_mut = mean(mass_mut,1);

    mass_dom = log_all{i,3}.singleCell.mass;
    av_mass_dom = mean(mass_dom(4:33,:),1);

    for j = 1:c
        if j == 3
            mass(j,:,i) = av_mass_dom;
           
        elseif j == 6
            mass(j,:,i) = av_mass_mut;
        else
        mass_test = log_all{i,j}.singleCell.mass;
        av_mass_test = mean(mass_test,1);
        mass(j,:,i) = av_mass_test;
        end
        
    D_ma(i,j) = distEu(av_mass_mut,mass(j,:,i));
    end
end

% volume
for i = 1:r
    
    volume_mut = log_all{i,6}.singleCell.volume;
    av_volume_mut = mean(volume_mut,1);

    volume_dom = log_all{i,3}.singleCell.volume;
    av_volume_dom = mean(volume_dom(4:33,:),1);

    for j = 1:c
        if j == 3
            volume(j,:,i) = av_volume_dom;
           
        elseif j == 6
            volume(j,:,i) = av_volume_mut;
        else
        volume_test = log_all{i,j}.singleCell.volume;
        av_volume_test = mean(volume_test,1);
        volume(j,:,i) = av_volume_test;
        end
        
    D_vo(i,j) = distEu(av_volume_mut,volume(j,:,i));
    end
end

% plots

for i = 1:r 
figure(i)

subplot(3, 1, 1);
plot(time, growth(6,:,i), 'r', time, growth(3,:,i), 'k', time, growth(1,:,i), 'b', ...
    time, growth(2,:,i), 'g', time, growth(4,:,i), 'y', time, growth(5,:,i), 'c');
title(['Growth for changes in the value ' parametryLabel{i}])
xlabel('time (s)');
ylabel('growth (g/s)');
legend('mutant','fit',num2str(parametryRK(i,1)),num2str(parametryRK(i,2)), ...
    num2str(parametryRK(i,4)),num2str(parametryRK(i,5)));

subplot(3, 1, 2);
plot(time, mass(6,:,i), 'r', time, mass(3,:,i), 'k', time, mass(1,:,i), 'b', ...
    time, mass(2,:,i), 'g', time, mass(4,:,i), 'y', time, mass(5,:,i), 'c');
title(['Mass for changes in the value ' parametryLabel{i}])
xlabel('time (s)');
ylabel('mass (g)');

subplot(3, 1, 3);
plot(time, volume(6,:,i), 'r', time, volume(3,:,i), 'k', time, volume(1,:,i), 'b', ...
    time, volume(2,:,i), 'g', time, volume(4,:,i), 'y', time, volume(5,:,i), 'c');
title(['Volume for changes in the value ' parametryLabel{i}])
xlabel('time (s)');
ylabel('volume (L)');

% saveas(gcf,'figure1','png')
% saveas(gcf,'figure1','fig')
end

nLinRegr(parametryRK, D_gr, D_ma, D_vo, r, parametryLabel)

