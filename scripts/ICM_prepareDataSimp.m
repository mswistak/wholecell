%ICM_prepareDataSimp
% Prepares 2 csv files containing parameters and distances of all jobs in target directory.
%
% Input:
%  - inputPath [char]: path to jobs' superdirectory (default is '../output/cloud/')
%  - outputPath [char]: (the default is '../data/')
%
% Output:
%  - H_simp.csv, H_simp.mat: files containing a 22xN matrix (N equals to number of jobs in target superdirectory)
%  - s_simp.csv, s_simp.mat: parameter distances to target mutant (1xN)
%  - labs_simp.mat: file containing cell with jobs' labels (for easy identification)
%


function [H, s, labs] = ICM_prepareDataSimp(inputPath, outputPath)


if (nargin < 1 | isempty(inputPath))
	inputPath = '../output/cloud/';
end

if (nargin < 2 | isempty(outputPath))
	outputPath = '../data/';
end

if (inputPath(end) ~= '/')
	inputPath = [inputPath '/'];
end

if (outputPath(end) ~= '/')
	outputPath = [outputPath '/'];
end

dirs = dir(inputPath);
dims = size(dirs);

H = [];
s = [];
labs = {};
for i = 1:(dims(1))
	try
		name = dirs(i).name;
		if (name(1) ~= '.')
			d = load([inputPath name '/' name '.distances.mat']);
			load([inputPath name '/' name '.alphaParameters.mat']);
			A = alphaMatrix(:);
			A = A([1:10 10+[1 3 4] 20+[2:10]]);
			if (any(A ~= 1))
				s = [s d.parameter];
				H = [H A];
				labs = [labs name];
			end
		end
	catch err
		fprintf('%s\n', err.message)
	end
end

csvwrite([outputPath 'H_simp.csv'], H)
save([outputPath 'H_simp.mat'], 'H')
csvwrite([outputPath 's_simp.csv'], s)
save([outputPath 's_simp.mat'], 's')
save([outputPath 'labs_simp.mat'], 'labs')


end
