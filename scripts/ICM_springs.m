%ICM_springs
%


function [result] = ICM_springs(varargin)


%Parsing input
ip = inputParser();

ip.addRequired('hooks', @isnumeric);
ip.addRequired('dists', @isnumeric);
ip.addParamValue('start', 0, @isnumeric );
ip.addParamValue('iters', 5000, @(x) isscalar(x) & isnumeric(x) );
ip.addParamValue('dumping', .01, @(x) isscalar(x) & isnumeric(x) );
ip.addParamValue('dt', .001, @(x) isscalar(x) & isnumeric(x) );
ip.addParamValue('maxSpeed', 1, @(x) isscalar(x) & isnumeric(x) );

ip.parse(varargin{:});

hooks = ip.Results.hooks;
dists = ip.Results.dists;
start = ip.Results.start;
iters = ip.Results.iters;
dumping = ip.Results.dumping;
dt = ip.Results.dt;
maxSpeed = ip.Results.maxSpeed;

if (start == 0)
	start = mean(hooks, 2);
elseif (size(hooks(:,1)) ~= size(start))
	throw(MException('ICM_springs:error', ...
			'Not matching start and hooks.'));
end

if (size(hooks(1,:)) ~= size(dists))
	throw(MException('ICM_springs:error', ...
			'Not matching dists and hooks.'));
end

[m, n] = size(hooks);

q = start;
qh = start;
sah = [];
v = zeros(size(q));
Eh = [];

for i = 1:iters
	%Current springs lengths
	sa = sum(log10(hooks ./ repmat(q,1,n)).^2, 1) / 1972;
	sah = [sah; sa];

	%Springs energy
	E = sum((sa-dists).^2);

	%Force caused by springs on our scanning mass
	f = sum(log10(hooks ./ repmat(q,1,n)) .* repmat(sa-dists,m,1), 2);

	%Yields such momentum...
	v = (1-dumping) .* v + f .* dt;
	u = sqrt(sum(v.^2));

	%Clip speed if necessary
	if (u > maxSpeed)
		v = v ./ u .* maxSpeed;
	end

	%... and such position change (both using Euler method)
	q = 10.^(log10(q) + v .* dt);


	%Some diagnostics
	if (mod(i, floor(iters/20)) == 0)
		fprintf('Timestep %d/%d, t=%g E=%g, f=%g v=%g\n', ...
		i, iters, (i-1)*dt, E, sqrt(sum(f.^2)), sqrt(sum(v.^2)));
	end

	qh = [qh q];
	Eh = [Eh E];
end

result = struct(...
	'estimate', q, ...
	'estimates', qh, ...
	'energies', Eh, ...
	'distances', sah ...
	);


end
