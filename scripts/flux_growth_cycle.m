% flux_growth_cycle
% preparing the simulation results
% input:
%   - reaction: with all metabolic simulation results
%   - gold: with simulation results whole cell mutant
%   - parametryLabel: parameter names
%   - w: vector changes in parameter values
% output:
%   - myfluxes: interesting fluxes corresponding changes in the parameters
%   - myfluxesGold: interesting fluxes corresponding changes in the
%   parameters (mutant)
%   - growth: growth for each of the five values of ten parameters
%   - CycleLength: cycle length for each of the five values ??of ten parameters


function [myfluxes myfluxesGold growth CycleLength] = flux_growth_cycle(reaction, gold, parametryLabel, w)
gen = 10;

fluxes = zeros(645,5,gen);
for k = 1:gen
    for j=1:length(w)
        fluxes(:,j,k) = reaction{k,j}{1,1};

    end
end

% my fluxes
myf = [];
myf_label = {};
k=1;
for i = 1:length(parametryLabel)
    for j = 1:length(gold.labels.rows.rxnFluxes)
    
        if isequal(parametryLabel(i),gold.labels.rows.rxnFluxes(j))==1
            myf(k) = j;
            myf_label(k) = gold.labels.rows.rxnFluxes(j);
            k=k+1;
        end
    
    end
end

myfluxes = zeros(length(parametryLabel),length(w),gen);
for i = 1:length(parametryLabel)
    myfluxes(i,:,:) = fluxes(myf(i),:,:);
end

% fluxes of mutant
myfluxesGold = [gold.rxnFluxes.mean(myf)];

% growth
growth = zeros(65000,5,gen);
for k = 1:gen
    for j=1:length(w)
        growth(:,j,k) = reaction{k,j}{1,2};
    end
end

% CellCycleLength
CycleLength = zeros(5,gen);
for k = 1:gen
    for j=1:length(w)
        CycleLength(j,k) = reaction{k,j}{1,3};
    end
end