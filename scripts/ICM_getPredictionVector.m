%ICM_getPredictionVector
% Create feature vector from predicted high-throughput data
%


function [predVec] = ICM_getPredictionVector(predStruct)

tmp = pwd();
cd('../WholeCell');
cleaner = onCleanup(@() cd(tmp));

setWarnings();
setPath();

sim = edu.stanford.covert.cell.sim.util.CachedSimulationObjectUtil.load();
time = sim.state('Time');
cellCycleLength = time.cellCycleLength;

predVec = struct('mean', [], 'std', []);
predVec.mean = [
	mean(nanmean(predStruct.singleCell.growth, 2), 1)
	mean(nanmean(predStruct.singleCell.mass, 2), 1)
	mean(nanmean(predStruct.singleCell.volume, 2), 1)
	
	mean(min(cellCycleLength, predStruct.singleCell.repInitTime))
	mean(min(cellCycleLength, predStruct.singleCell.repTermTime))
	mean(min(cellCycleLength, predStruct.singleCell.cellCycleLen))

	predStruct.metConcs.mean
	predStruct.dnaSeq.mean
	predStruct.rnaSeq.mean
	predStruct.chipSeq.mean(:)
	predStruct.rnaArray.mean
	predStruct.protArray.mean
	predStruct.rxnFluxes.mean
	];
predVec.std = [
	sqrt(mean(nanvar(predStruct.singleCell.growth, 1, 2)))
	sqrt(mean(nanvar(predStruct.singleCell.mass, 1, 2)))
	sqrt(mean(nanvar(predStruct.singleCell.volume, 1, 2)))

	std(min(cellCycleLength, predStruct.singleCell.repInitTime))
	std(min(cellCycleLength, predStruct.singleCell.repTermTime))
	std(min(cellCycleLength, predStruct.singleCell.cellCycleLen))

	predStruct.metConcs.std
	predStruct.dnaSeq.std
	predStruct.rnaSeq.std
	predStruct.chipSeq.std(:)
	predStruct.rnaArray.std
	predStruct.protArray.std
	predStruct.rxnFluxes.std
	];


end
