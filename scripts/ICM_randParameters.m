%ICM_randParameters()
% Computes random transformation matrix according to DREAM8 Challenge specification.
%
% Output:
%  - randAlphaMatrix [double]: represents a random transformation matrix for parameters in DREAM8 Challenge
%


function [randAlphaMatrix] = ICM_randParameters()


%4 in first column
whichRowsInColumns = {randsample(1:10, 4)};

%in second column one must be out in order to satisfy requirments (5 genes with 1 associated change and 5 genes with 2 associated changes)
not_in_second = randsample(whichRowsInColumns{1}, 1);

%4 in second column (out of the remaining)
whichRowsInColumns = [whichRowsInColumns; randsample([1:(not_in_second-1) (not_in_second+1):10], 4)];

f_s_intersect = intersect(whichRowsInColumns{1}, whichRowsInColumns{2});
f_s_union = union(whichRowsInColumns{1}, whichRowsInColumns{2});

%these must be changed in third column
remaining = setdiff(1:10, f_s_union);

%from these we will choose the rest of third column (thanks to this there will be no genes with 3 associated changes)
f_s_diff= setdiff(f_s_union, f_s_intersect);

%7 in third column
whichRowsInColumns = [whichRowsInColumns; union(remaining, randsample(f_s_diff, 7-length(remaining)))];

%random alphas in selected positions; all in range 0-1
randAlphaMatrix = ones(10,3);
randAlphaMatrix(whichRowsInColumns{1},1) = rand(4,1);
randAlphaMatrix(whichRowsInColumns{2},2) = rand(4,1);
randAlphaMatrix(whichRowsInColumns{3},3) = rand(7,1);

%choose 2 mutations with alphas in range 1-2
j = randi(3, 1, 2);

%'up' changes are in the same column
if (j(1) == j(2))
	i = randsample(whichRowsInColumns{j(1)}, 2);
%'up' changes are in different columns
else
	i = [];
	for c = j
		i = [i randsample(whichRowsInColumns{c}, 1)];
	end
end

%2 random alphas in range 1-2
for c = 1:2
	randAlphaMatrix(i(c),j(c)) = 1+rand;
end


end
