% regression
% function generating plots of regression
% input:
%   - parametry: all changed parameter values
%   - myfluxes: interesting fluxes corresponding to changes in the parameters
%   - growth: growth for each of five values of each of ten parameters
%   - parametryLabel: parameter names
%   - w: changes' vector in parameter values
%   - linORlog: linear or logarithmic regression (1 or 2)
% output:
%   - korFlux: regression, correlation coefficient (fluxes)
%   - deterFlux: coefficient of determination
%   - korGrowth: regression, correlation coefficient (growth)
%   - deterGrowth: coefficient of determination

function [korFlux deterFlux, korGrowth deterGrowth] = regression(parametry, myfluxes, growth, parametryLabel, w, linORlog)

gen = [1:10];
% linear- fluxes
if linORlog == 1
korFlux = zeros(length(gen),length(parametryLabel));
deterFlux = zeros(length(gen),length(parametryLabel));
for ge = 1:length(gen)
    figure()
    for fl = 1:length(parametryLabel)
        
        [B0 B1 Yprzew R2 r y_przew_gor y_przew_dol] = regresja(parametry(gen(ge),1:length(w))',...
            myfluxes(fl,1:length(w),gen(ge))',linORlog); % myfluxes
        korFlux(gen(ge),fl) = r;
        deterFlux(gen(ge),fl) = R2;
       
        subplot(5,2,fl)
        plot (parametry(gen(ge),1:length(w))',Yprzew,'b', parametry(gen(ge),1:length(w))',...
            y_przew_gor,'m', parametry(gen(ge),1:length(w))',y_przew_dol,'m', parametry(gen(ge),1:length(w))',...
            myfluxes(fl,1:length(w),gen(ge))','*g')
        if fl == 1
            legend('regression line','confidence intervals a=0.05')
        end
        title(['Linear regression ' parametryLabel{gen(ge)}])
        xlabel('parameter');
        ylabel(['flux ' parametryLabel{fl} ' (rxn s^{-1})']);
%         saveas(gcf,['flux_wykr\' parametryLabel{gen(ge)} '_' parametryLabel{fl}],'png')
    end
end
end

% logarithmic- fluxes
if linORlog == 2
korFlux = zeros(length(gen),length(parametryLabel));
deterFlux = zeros(length(gen),length(parametryLabel));

for ge = 1:length(gen)
    figure()
    for fl = 1:length(parametryLabel)
        
        [B0 B1 Yprzew R2 r y_przew_gor y_przew_dol] = regresja(parametry(gen(ge),1:length(w))',...
            myfluxes(fl,1:length(w),gen(ge))',linORlog); % myfluxes
        korFlux(ge,fl) = r;
        deterFlux(ge,fl) = R2;
        
        subplot(5,2,fl)
        plot (parametry(gen(ge),1:length(w))',Yprzew,'b', parametry(gen(ge),1:length(w))',...
            y_przew_gor,'m', parametry(gen(ge),1:length(w))',y_przew_dol,'m', parametry(gen(ge),1:length(w))',...
            myfluxes(fl,1:length(w),gen(ge))','*g')
        if fl == 1
            legend('regression line','confidence intervals a=0.05')
        end
        title(['Logarithmic regression ' parametryLabel{gen(ge)}])
        xlabel('parameter');
        ylabel(['flux ' parametryLabel{fl} ' (rxn s^{-1})']);
%         saveas(gcf,['flux_wykr_log\' parametryLabel{gen(ge)} '_' parametryLabel{fl}],'png')
    end
end
end

% linear- growth
if linORlog == 1
korGrowth = [];
deterGrowth = [];

figure()
for ge = 1:length(parametryLabel)

    [B0 B1 Yprzew R2 r y_przew_gor y_przew_dol] = regresja(parametry(ge,:)', ...
        mean(growth(:,:,ge),1)',linORlog); % growth
    korGrowth(ge) = r;
    deterGrowth(ge) = R2;
    
    subplot(5,2,ge)
    plot(parametry(ge,:)',Yprzew,'b', parametry(ge,:)',y_przew_gor,'m', ...
        parametry(ge,:)',y_przew_dol,'m', parametry(ge,:)',mean(growth(:,:,ge),1)','*g')
    if ge == 1
        legend('regression line','confidence intervals a=0.05')
    end
    title(['Linear regression ' parametryLabel{ge}])
    xlabel('parameter');
    ylabel('average growth');
%     saveas(gcf,['growth_wykr\' parametryLabel{ge}],'png')
end
end

% logarithmic- growth
if linORlog == 2
korGrowth = [];
deterGrowth = [];

figure()
for ge = 1:length(parametryLabel)

    [B0 B1 Yprzew R2 r y_przew_gor y_przew_dol] = regresja(parametry(ge,:)', ...
        mean(growth(:,:,ge),1)',linORlog); % growth
    korGrowth(ge) = r;
    deterGrowth = R2;
    
    subplot(5,2,ge)
    plot (parametry(ge,:)',Yprzew,'b', parametry(ge,:)',y_przew_gor,'m', ...
        parametry(ge,:)',y_przew_dol,'m', parametry(ge,:)',mean(growth(:,:,ge),1)','*g')
    if ge == 1 
        legend('regression line', 'confidence intervals a=0.05')
    end
    title(['Logarithmic regression ' parametryLabel{ge}])
    xlabel('parameter');
    ylabel('average growth');
%     saveas(gcf,['growth_wykr_log\' parametryLabel{ge}],'png')
end
end
