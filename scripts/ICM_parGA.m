%ICM_parGA
% Genetic algorithm.
%
% Input:
%  - refDists [double]: reference distances
%  - refDistsPaths [double]: reference distances paths
%  - refAMs [cell]: reference alphaMatrices
%  - refAMsPaths [cell]: reference alphaMatrices paths
%  - outputName [char]: the default is current date and time
%  - outputPath [char]: the default is '../output/GA/'
%  - max_n [integer]: maximum population
%  - max_it [integer]: maximum iteration
%  - elite [double]: share of population passing to next generation
%  - parent [double]: share of population having offspring
%  - mutationRate [integer]: number of mutations between generations
%  - noWrite [integer]: flag for writing output
%
% Output:
%  - dists: distances to reference parameter values
%  - aMs [cell]: alphaMatrices describing parameter values' transformations
%
% All output is sorted according to ascending distance differences compared to refDist.
%

function [dists, aMs] = ICM_parGA(varargin)


%%Parsing input
ip = inputParser();

ip.addParamValue('refDists', -1, @(x) all(isnumeric(x) & all(x > 0)) ); 
ip.addParamValue('refDistsPaths', '../output/cloud/fit/fit.distances.mat', @(x) ischar(x) | iscell(x) ); 
ip.addParamValue('refAMs', {ones(10,3)}, @iscell);
ip.addParamValue('refAMsPaths', '', @(x) ischar(x) | iscell(x) ); 
ip.addParamValue('outputName', '', @ischar); 
ip.addParamValue('outputPath', '../output/GA/', @ischar); 
ip.addParamValue('max_n', 10000, @(x) isnumeric(x) & isscalar(x) & (x > 0) );
ip.addParamValue('max_it', 1000, @(x) isnumeric(x) & isscalar(x) & (x > 0) );
ip.addParamValue('elite', .1, @(x) isnumeric(x) & isscalar(x) & (x > 0) & (x < 1) );
ip.addParamValue('parent', .5, @(x) isnumeric(x) & isscalar(x) & (x > 0) & (x < 1) );
ip.addParamValue('mutationRate', 5, @(x) isnumeric(x) & all(x > 0) );
ip.addParamValue('noWrite', 0);

ip.parse(varargin{:});

refDists = ip.Results.refDists;
refDistsPaths = ip.Results.refDistsPaths;
refAMs = ip.Results.refAMs;
refAMsPaths = ip.Results.refAMsPaths;
outputName = ip.Results.outputName;
outputPath = ip.Results.outputPath;
max_n = ip.Results.max_n;
max_it = ip.Results.max_it;
elite = ip.Results.elite;
parent = ip.Results.parent;
mutationRate = ip.Results.mutationRate;
noWrite = ip.Results.noWrite;

if (iscell(refDistsPaths))
	refDists = [];
	for i = 1:length(refDistsPaths)
		refDist = load(refDistsPaths{i});
		refDists = [refDists refDist.parameter];
	end
elseif (all(refDists >= 0))
	;
elseif (ischar(refDistsPaths))
	refDists = load(refDistsPaths);
	refDists = refDists.parameter;
end
if (any(refDists < 0))
	throw(MException('ICM_GA:error', ...
		'Missing required parameter. Please ensure that reference distances and alphaMatrices are specified.'));
end
if (~isempty(refAMsPaths))
	if (iscell(refAMsPaths))
		refAMs = {};
		for i = 1:length(refAMsPaths)
			load(refAMsPaths{i});
			refAMs = [refAMs alphaMatrix];
		end
	end
end
if (length(refDists) ~= length(refAMs))
	throw(MException('ICM_GA:error', ...
		'Lengths of required parameters ("refDists", "refAMs") do not match.', ...
		'They contain %d and %d values respectively', length(refDists), length(refAMs)));
end

if (outputPath(end) ~= '/')
	outputPath = [outputPath '/'];
end

if (isempty(outputName))
	outputName = datestr(now, 'yyyy_mm_dd_HH_MM_SS');
end

if (elite > parent)
	parent = elite;
end

if (isscalar(mutationRate))
	mutationRate = repmat(mutationRate, 1, max_it);
elseif (length(mutationRate) ~= max_it)
	mutationRate = repmat(mutationRate(1), max_it);
end
mutationRate = ceil(mutationRate);

elite_n = round(elite*max_n);
parent_n = round(parent*max_n);
if (mod(elite_n,2) ~= 0)
	elite_n = elite_n+1;
end
elite = 1:elite_n;
if (mod(parent_n,2) ~= 0)
	parent_n = parent_n+1;
end
parent = 1:parent_n;


aMs = {};
dists = [];
ranks = 1:max_n;
missing_n = max_n;

%%Begin algorithm
for i = 1:max_it

	%elite & offspring
	if (missing_n ~= max_n)
		p_aMs = aMs(:,ranks(parent));
		
		aMs = aMs(:,ranks(elite));
		dists = dists(:,ranks(elite));
		rand_parent = randperm(parent_n);
		for k = 1:(parent_n/2)
			[c1_aM, c2_aM] = ICM_reproduce(...
				p_aMs{rand_parent(2*k-1)}, ...
				p_aMs{rand_parent(2*k)}, ...
				mutationRate(i) ...
				);

			c1_dist = [];
			c2_dist = [];
			for j = 1:length(refAMs)
				c1_dist = [c1_dist; ICM_calcDist(c1_aM, refAMs{j})];
				c2_dist = [c2_dist; ICM_calcDist(c2_aM, refAMs{j})];
			end

			aMs = [aMs c1_aM c2_aM];
			dists = [dists c1_dist c2_dist];
		end
	end

	%new random
	for k = 1:missing_n
		new_aM = ICM_randParameters();
		new_dist = [];
		for j = 1:length(refAMs)
			new_dist = [new_dist; ICM_calcDist(new_aM, refAMs{j})];
		end

		aMs = [aMs new_aM];
		dists = [dists new_dist];
	end

	%rank
	diff = [];
	for j = 1:length(refDists)
		diff = [diff; abs(dists(j,:) - refDists(j))];
	end
	cum_diff = sum(diff,1);
	[~, ranks] = sort(cum_diff);
	fprintf('Best in interation %d:\n', i);
	for j = 1:length(refDists)
		fprintf('\t%f, %f, %f, %f, %f\n', dists(j,ranks(1:5)));
	end

	missing_n = max_n - (elite_n + parent_n);
end


%%Output in ascending dists' order
dists = dists(:,ranks);
aMs = aMs(ranks);


%%Save output to file
if (noWrite == 0)
	if(~exist(outputPath,'dir'))
		mkdir(outputPath);
	end
	save([outputPath outputName '_i_' num2str(max_it) '_n_' num2str(max_n) '_m_' int2str(mutationRate(1)) '.mat'], 'dists', 'aMs');
end







end
