%ICM_pointMutation
% Introduces point mutation in specified location of an alphaMatrix.
%
% Input:
%  - alphaMatrix
%  - i, j: indices (optional)
%
% Output:
%  - alphaMatrix: mutated input
%


function [alphaMatrix] = ICM_pointMutation(alphaMatrix, i, j)

t = 1;
if (nargin < 3)
	while (t == 1)
		i = randi(10,1,1);
		j = randi(3,1,1);
		t = alphaMatrix(i,j);
	end
end

if (alphaMatrix(i,j) > 1)
	k = 1;
else
	k = 0;
end

alphaMatrix(i,j) = k + rand;


end
