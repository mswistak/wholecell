% metabolAnalysis
% analysis of simulation results of metabolic submodel

% load all_reaction with all metabolic simulation results
% load gold.predictions with simulation results of whole cell mutant
% load parameters with default values of parameters
% select linORlog- linear or logarithmic regression

clear all
close all
clc

linORlog = 2; % linear regression linORlog = 1, 
              % logarithmic regression linORlog = 2

% load('/Documents/Praktyka-ICM/input/metabolic/all_reaction.mat');
% gold = load('/Documents/Praktyka-ICM/input/gold/gold.predictions.mat');
% allParameters = load('/Documents/Praktyka-ICM/input/parameters.mat');
load('../output/metabolic/all_reaction.mat');
gold = load('../data/gold.predictions.mat');
allParameters = load('../output/single/parameters.mat');

pRK = allParameters.pRK;

w = [0.1 0.5 1 2 10]; % changes' vector in parameter values
% parameter values
Tmk = w.*pRK(1); %0.07
Fba = w.*pRK(2);
MetK = w.*pRK(3);
Pgi = w.*pRK(4);
AceE = w.*pRK(5);
Pta = w.*pRK(6);
CmkA2 = w.*pRK(7);
AckA = w.*pRK(8);
Eno = w.*pRK(9);
TpiA = w.*pRK(10);

parametryLabel = {'Tmk' 'Fba' 'MetK' 'Pgi' 'AceE' 'Pta' 'CmkA2' 'AckA' 'Eno' 'TpiA'};
parametry = [Tmk; Fba; MetK; Pgi; AceE; Pta; CmkA2; AckA; Eno; TpiA];

% function preparing the simulation results
[myfluxes myfluxesGold growth CycleLength] = flux_growth_cycle(reaction, gold, parametryLabel, w);

% regression
[korFlux deterFlux, korGrowth deterGrowth] = regression(parametry, myfluxes, growth, parametryLabel, w, linORlog);




