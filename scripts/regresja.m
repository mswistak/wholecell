% regresja
% function performing regression
% input:
%   - X: vector of parameter values
%   - Y: myfluxes or growth vector
%   - linORlog: linear or logarithmic regression (1 or 2)
% output:
%   - B0: model coefficients
%   - B1: model coefficients
%   - Yprzew: dependent variable
%   - R2: coefficient of determination
%   - r: correlation coefficient
%   - y_przew_gor: upper confidence interval
%   - y_przew_dol: lower confidence interval

function [B0 B1 Yprzew R2 r y_przew_gor y_przew_dol] = regresja(X,Y,linORlog)

x = [Y X];
[X,I]=sort(X);
Y=Y(I);
[w, k]=size(x);
n=w;
p=k;
ys=mean(Y);
xs=mean(X);

if linORlog == 1
X=[ones(n,1) X];
elseif linORlog == 2
X=[ones(n,1) log(X)];
end

B=((X'*X)^-1)*(X'*Y); % method of least squares
% model coefficients I
B0=B(1);
B1=B(2);
X = X(:,2);

sx=std(X);
sy=std(Y);

% or model coefficients II
% B1=r*(sy/sx);
% B0=ys-B1*xs;

% correlation coefficient
r=sum((X-xs).*(Y-ys))/sqrt(sum((X-xs).^2)*sum((Y-ys).^2));
% or
% r = B1*(sx/sy);

Yprzew=B1*X + B0; % dependent variable

e = Y - Yprzew;
SSE = sum(e.^2);
SSR = sum((Yprzew-ys).^2);
SST = SSE + SSR;
S2 = SSE / (n-2); % mean square error
R2=SSR/SST; % coefficient of determination

% confidence intervals for the model coefficients
% alpha=0,05.
alfa=0.05;
t=tinv(1-0.5*alfa, n-2);
SE_B0=sqrt(S2*(1/n+xs^2/(sum((X-xs).^2))));
SE_B1=sqrt(S2/(sum((X-xs).^2)));
B0_gor=B0+t*SE_B0;
B0_dol=B0-t*SE_B0;
B1_gor=B1+t*SE_B1;
B1_dol=B1-t*SE_B1;

% confidence intervals for the expected values
SEy_przew =sqrt(S2*(1/n+(X-xs).^2./sum((X-xs).^2)));
y_przew_gor=Yprzew+t*SEy_przew;
y_przew_dol=Yprzew-t*SEy_przew;

end
