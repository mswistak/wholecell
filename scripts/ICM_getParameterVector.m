%ICM_getParameterVector
% Create feature vector from full set of parameters
%


function [paramVec] = ICM_getParameterVector(paramStruct)

tmp = pwd();
cd('../WholeCell');
cleaner = onCleanup(@() cd(tmp));

setWarnings();
setPath();

sim = edu.stanford.covert.cell.sim.util.CachedSimulationObjectUtil.load();
rna = sim.state('Rna');
met = sim.process('Metabolism');
trn = sim.process('Transcription');

halfLives = rna.halfLives;
enzBounds = met.enzymeBounds;
tuBindProbs = trn.transcriptionUnitBindingProbabilities;

if isfield(paramStruct, 'states') && isfield(paramStruct.states, 'Rna') && isfield(paramStruct.states.Rna, 'halfLives')
	halfLives = paramStruct.states.Rna.halfLives;
end
if isfield(paramStruct, 'processes') && isfield(paramStruct.processes, 'Metabolism') && isfield(paramStruct.processes.Metabolism, 'enzymeBounds')
	enzBounds = paramStruct.processes.Metabolism.enzymeBounds;
end
if isfield(paramStruct, 'processes') && isfield(paramStruct.processes, 'Transcription') && isfield(paramStruct.processes.Transcription, 'transcriptionUnitBindingProbabilities')
	tuBindProbs = paramStruct.processes.Transcription.transcriptionUnitBindingProbabilities;
end

paramVec = [
	halfLives(rna.matureIndexs)
	-enzBounds(:, 1)
	enzBounds(:, 2)
	tuBindProbs
	];


end
