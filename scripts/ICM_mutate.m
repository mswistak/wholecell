%ICM_mutate
% Applies n random mutations to alphaMatrix.
%
% Input:
%  - alphaMatrix
%  - mutationRate [integer]: number of mutations applied to alphaMatrix
%
% Output:
%  - alphaMatrix: mutated input
%


function alphaMatrix = ICM_mutate(alphaMatrix, mutationRate)

if (nargin < 2)
	mutationRate = 1;
end

for k = 1:mutationRate
	switch randi(3,1,1)
		case 1
			alphaMatrix = ICM_rowSwapMutation(alphaMatrix);
		case 2
			alphaMatrix = ICM_swap2ColumnsIn2RowsMutation(alphaMatrix);
		case 3
			alphaMatrix = ICM_pointMutation(alphaMatrix);
	end
end


end
