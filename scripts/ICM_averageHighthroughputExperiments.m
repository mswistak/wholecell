%ICM_averageHighthroughputExperiments
% Averages multiple in silico high-throughput experiments.
%
% Input:
% - simPathPattern [.mat file path pattern]: file path to .mat files
% - avgValsPath [.mat file path]: Location where average values should be saved in .mat format
% - verbosity [integer]: Desired verbosity level. Zero supresses output. Higher value prints more output.
%
% Output:
% - avgVals [struct]: Struct containing average value of in silico experimental data
% - labels [struct]: Struct containing row and column labels for avgVals
% - If avgValsPath is set, saves average simulated in silico experimental data to .mat file
%


function [avgVals, labels] = ICM_averageHighthroughputExperiments(varargin)

tmp = pwd();
cd('../WholeCell');
cleaner = onCleanup(@() cd(tmp));

setWarnings();
setPath();

try
	[avgVals, labels] = averageHighthroughputExperiments(varargin{:});
catch err
	rethrow(err);
end


end
