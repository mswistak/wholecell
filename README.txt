##############################README###############################


This repo documents the participation of ICM_Poland_dreamteam in "Whole-cell parameter 
estimation DREAM challenge".

We deploy here code used in the challenge, obtained output as well as decriptions of 
predictions' methods and usage of sorce code.

We hope it'll all be understandable.

If there are any problems please contact us.



###################################################################
DIR Tree

./
    README.txt
     (the following document)

    installation_notes.txt
     (notes on downloading the whole-cell model code and it's installation)

    ICM_methods.pdf
     (document describing our whole methodology)


./analysis/
    (summaries of the analysis of experimental data)

    /figures/
        (visualizations of data analysis)


./data/
    (additional data files taken into account during data analysis; output of supporting scripts)

./docs/
    (WholeCell model documetation)

./output/
    (obtained simulation output: predictions, distances, parameters' structures)

    /cloud/
        (output of simulations run using organizers' provided bitmill computing power)

    /GA/
        (output of genetic algorithms)

    /<others>/
        (output of locally performed simulations)


./scripts/
    (source code developed for solving the challenge)


./WholeCell/
    (whole-cell model source code)



###################################################################
Functions' usage
	
runParGA.pbs
    (example pbs script for running parameter-distance-based GA on computer cluster)

runSim.pbs
    (example pbs script for running simulations on computer cluster)

##########

1. Metabolic submodel simulation
	metabolicSim.m
------------------------------------------------------------------------
	- Enter the path of WholeCell model
	- Enter the simulation time
	- Enter the simulation batch output directory
	- mat-files with the simulation results are being saved in the directory 'simBatchDir'.
________________________________________________________________________

2. Insertion of the script to the queue on the halo2
	job.pbs
------------------------------------------------------------------------
	#PBS -N 
	e.g. #PBS -N MyJob (new job)
	#PBS -q halo2 
	(queue on halo2)
	#PBS -A 
	e.g. #PBS -A G01-77 (user computing grant number)
	#PBS -l nodes=2:ppn=16 
	(Booking two nodes, each of which has 16 processors)
	#PBS -l mem=10gb 
	(The total amount of memory allocated to all processes)
	#PBS -l walltime=300:00:00 
	(The maximum length of the task action)
	/path/to/the/program -nosplash -nodesktop -nodisplay < script.m

	Insert job to the queue: qsub job.pbs
________________________________________________________________________

3. Writing submodel simulation results into a single structure- reaction
   	metabolicLog.m
------------------------------------------------------------------------
	- Enter the path of WholeCell model
	- Enter the path 'path' to the metabolic submodel simulation results files
	- Run it in the matlab.
________________________________________________________________________

4. Preparing and running simulations of whole cells- single parameter changes
	allSim.m
------------------------------------------------------------------------
	- Select the type of the parameter
	- Select the indices of genes
	- Enter the path of WholeCell model
	- Enter the simulation batch output directory (variable 'simBatchDir')
	- Simulation results are being saved in the directory 'simBatchDir'
	- Script saves default parameter values vectors in the indicated directory. 
________________________________________________________________________

5. Insertion of the script allSim.m to the queue on the halo2
   look point 2
________________________________________________________________________

6. Analysis of simulation results of metabolic submodel
	metabolAnalysis.m
------------------------------------------------------------------------
	- Select 'linORlog' 1 or 2
	- Load all_reaction.mat, gold.predictions.mat and parameters.mat
	- The program uses the functions 
		flux_growth_cycle()- preparing the simulation results
		regression()- generating plots of regression and 
		regresja()- performing regression
	- Run it in the matlab.
________________________________________________________________________

7. Preparation of simulation results and graphical presentation
	gold_fit.m
------------------------------------------------------------------------
	- Load fit.mat and gold.predictions.mat
	- Enter the simulation time
	- Run it in the matlab.
________________________________________________________________________

8. Comparison of simulation results for changes in the value 
   of individual parameters; stability parameters
	singleParameters.m
------------------------------------------------------------------------
	- Load parameters.mat, fit.mat and gold.predictions.mat
	- Enter path to the simulation results
	- The program uses the functions 
		distEu()- Euclidean distance calculation 
		nLinRegr()- performing nonlinear regression
	- Run it in the matlab.
________________________________________________________________________
